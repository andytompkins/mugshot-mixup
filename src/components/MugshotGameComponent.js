'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import MugshotRowComponent from 'components/MugshotRowComponent';
import GameOverComponent from 'components/GameOverComponent';
import Game from '../game.js';

require('styles//MugshotGame.css');

let bg = require('../images/lineup.jpg');
let moveSound = new Audio('../sounds/slide.mp3');
let changeReelSound = new Audio('../sounds/pop.wav');

const mugshots = 14;
const pieces = 3;

let mugs = new Array(pieces);
for (var i = 0; i < pieces; i++) {
  mugs[i] = new Array(mugshots);
  let pos = '';
  if (i == 0) {
    pos = 'top';
  } else if (i == 1) {
    pos = 'mid';
  } else {
    pos = 'bot';
  }
  for (var j = 1; j <= mugshots; j++) {
    let filename = 'mug' + j + '-' + pos + '.png';
    let image = require('../images/faces/' + filename);
    mugs[i][j-1] = {
      'image': image,
      'mug': j
    };
  }
}
//var game = new Game(mugs, 10);

class MugshotGameComponent extends React.Component {
  
  constructor(props) {
    super(props);
    
    var game = new Game(mugs, this.props.difficulty);
    this.state = {
      'game': game,
      'time': 30
    };
    
    this.newGame = this.newGame.bind(this);
    this.tick = this.tick.bind(this);
    this.handleKeyUp = this.handleKeyUp.bind(this);
    
  }
  
  newGame() {
    var game = new Game(mugs, this.props.difficulty);
    this.setState({ 'game': game, 'time': 30 });
  }
  
  tick() {
    if (this.state.game.over === true) {
      window.clearInterval(this.state.interval);
    }
    this.state.time--;
    if (this.state.time === 0) {
      this.state.game.game_over(false);
      window.clearInterval(this.state.interval);
    }
    this.setState({ 'game': this.state.game });
  }
  
  componentDidMount() {
    this.state.interval = window.setInterval(this.tick, 1000);
    ReactDOM.findDOMNode(this.refs.inputDiv).focus();
  }
  
  handleKeyUp(e) {
    if(!this.state.game.over) {
      
      if (e.which === 38 || e.which === 87) {
        changeReelSound.play();
        this.state.game.reel_up();
      } else if (e.which === 40 || e.which === 83) {
        changeReelSound.play();
        this.state.game.reel_down();
      } else if (e.which === 39 || e.which === 68) {
        moveSound.play();
        this.state.game.reel_right();
      } else if (e.which === 37 || e.which === 65) {
        moveSound.play();
        this.state.game.reel_left();
      }
      this.setState({ 'game': this.state.game });
      
    } else {
      
      if (e.which === 89) {
        this.newGame();
      } else if (e.which === 78) {
        this.props.ender();
      }
      
    }
    
  }
  
  render() {
    return (
      <div ref='inputDiv' tabIndex={0} className="index" onKeyUp={this.handleKeyUp} style={{'backgroundImage': 'url(' + bg + ')'}}>
        <span className="timer">:{this.state.time}</span>
      
        <table className="faces">
          <tbody>
            <MugshotRowComponent row={0} class="face-top" game={this.state.game} />
            <MugshotRowComponent row={1} class="face-mid" game={this.state.game} />
            <MugshotRowComponent row={2} class="face-bot" game={this.state.game} />
          </tbody>
        </table>
        
        <GameOverComponent game={this.state.game} />
        
      </div>
      
    );
  }
}

MugshotGameComponent.displayName = 'MugshotGameComponent';

// Uncomment properties you need
MugshotGameComponent.propTypes = {
  'difficulty': React.PropTypes.number,
  'ender': React.PropTypes.func
};
MugshotGameComponent.defaultProps = {
  'difficulty': 10,
  'ender': null
};

export default MugshotGameComponent;
