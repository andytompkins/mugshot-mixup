require('normalize.css');
require('styles/App.css');

import React from 'react';
import MugshotGameComponent from 'components/MugshotGameComponent';
import SplashComponent from 'components/SplashComponent';

class AppComponent extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      playing: false,
      difficulty: 10
    };
    
    this.startGame = this.startGame.bind(this);
    this.endGame = this.endGame.bind(this);
  }
  
  startGame(difficulty) {
    this.setState({ 'difficulty': difficulty, 'playing': true });
  }
  
  endGame() {
    this.setState({ 'playing': false });
  }
  
  render() {
    if (this.state.playing) {
      return (
        <MugshotGameComponent difficulty={this.state.difficulty} ender={this.endGame} />
      );
    }
    return (
      <SplashComponent starter={this.startGame} />
    );
  }
  
}

AppComponent.propTypes = {};
AppComponent.defaultProps = {
};

export default AppComponent;
