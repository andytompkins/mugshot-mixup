'use strict';

import React from 'react';

require('styles/Cursor.css');

class CursorComponent extends React.Component {
  constructor() {
    this.state = {
      current_reel = 1;
    };
  }
  render() {
    return (
      <div className="cursor-component">
      </div>
    );
  }
}

CursorComponent.displayName = 'CursorComponent';

// Uncomment properties you need
// CursorComponent.propTypes = {};
// CursorComponent.defaultProps = {};

export default CursorComponent;
