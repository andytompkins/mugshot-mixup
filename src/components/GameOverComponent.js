'use strict';

import React from 'react';

require('styles/GameOver.css');

let win = require('../images/win.png');
let lose = require('../images/lose.png');

class GameOverComponent extends React.Component {
  
  constructor(props) {
    super(props);
  }
  
  render() {
    if (this.props.game.over === true) {
      if (this.props.game.won === true) {
        return (
          <div>
            <img className='win' src={win} />
            <p className='play-again'>Play again? y/n</p>
          </div>
        );
      } else {
        return (
          <div>
            <img className='lose' src={lose} />
            <p className='play-again'>Play again? y/n</p>
          </div>
        );
      }
    }
    return (
      <div>
      </div>
    );
  }
}

GameOverComponent.displayName = 'GameOverComponent';

// Uncomment properties you need
// GameOverComponent.propTypes = {};
// GameOverComponent.defaultProps = {};
GameOverComponent.propTypes = {
  'game': React.PropTypes.object
};
GameOverComponent.defaultProps = {
  'game': null
};

export default GameOverComponent;
