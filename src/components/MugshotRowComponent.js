'use strict';

import React from 'react';

import MugshotComponent from 'components/MugshotComponent';

require('styles//MugshotRow.css');

class MugshotRowComponent extends React.Component {
  
  constructor(props) {
    super(props);
  }
  
  render() {
    return (
      <tr className={this.props.class}>
        <MugshotComponent row={this.props.row} col={this.props.game.puzzle[0].length - 1} game={this.props.game} class="row-start" />
        <MugshotComponent row={this.props.row} col={0} game={this.props.game} />
        <MugshotComponent row={this.props.row} col={1} game={this.props.game} />
        <MugshotComponent row={this.props.row} col={2} game={this.props.game} />
        <MugshotComponent row={this.props.row} col={3} game={this.props.game} class="row-end" />
      </tr>
    );
  }
}

//<td><img src={this.props.game.puzzle[this.props.row][0].image} /></td>
//<td><img src={this.props.game.puzzle[this.props.row][1].image} /></td>
//<td><img src={this.props.game.puzzle[this.props.row][2].image} /></td>

MugshotRowComponent.displayName = 'MugshotRowComponent';

// Uncomment properties you need
MugshotRowComponent.propTypes = {
  'row': React.PropTypes.number,
  'class': React.PropTypes.string,
  'game': React.PropTypes.object
};
MugshotRowComponent.defaultProps = {
  'row': 0,
  'class': '',
  'game': null
};

export default MugshotRowComponent;
