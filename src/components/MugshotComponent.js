'use strict';

import React from 'react';

require('styles//Mugshot.css');

class MugshotComponent extends React.Component {
  
  constructor(props) {
    super(props);
    this.class = this.props.class;
  }
  
  render() {
    let sel = '';
    if (this.props.col === 1 && this.props.game.selected_reel === this.props.row) {
      //this.class = this.props.class + ', selection';
      sel = 'selection';
    } else {
      //this.class = this.props.class;
      sel = '';
    }
    return (
      <td className={sel}><img className={this.class} src={this.props.game.puzzle[this.props.row][this.props.col].image} /></td>
    );
  }
}

MugshotComponent.displayName = 'MugshotComponent';

// Uncomment properties you need
MugshotComponent.propTypes = {
  'row': React.PropTypes.number,
  'col': React.PropTypes.number,
  'class': React.PropTypes.string,
  'game': React.PropTypes.object
};
MugshotComponent.defaultProps = {
  'row': 0,
  'col': 0,
  'class': '',
  'game': null
};
export default MugshotComponent;
