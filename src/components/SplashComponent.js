'use strict';

import React from 'react';
import ReactDOM from 'react-dom';

require('styles/Splash.css');
let bg = require('../images/mm.png');
let mugshot = require('../images/mugshot.png');
let mixup = require('../images/mixup.png');

class SplashComponent extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {
      'difficulty': 10
    };

    this.handleKeyUp = this.handleKeyUp.bind(this);
  }
  
  componentDidMount() {
    ReactDOM.findDOMNode(this.refs.inputDiv).focus();
  }
  
  handleKeyUp(e) {
    let diff = 0;
    //console.log('splash got key ' + e.which);
    if (e.which === 13) {
      this.props.starter(this.state.difficulty);
    } else if (e.which === 38 || e.which === 87) {
      diff = this.state.difficulty - 2;
      if (diff < 10) { diff = 10; }
      this.setState({ 'difficulty': diff });
    } else if (e.which === 40 || e.which === 83) {
      diff = this.state.difficulty + 2;
      if (diff > 14) { diff = 14; }
      this.setState({ 'difficulty': diff });
    } 
  }
  
  render() {
    let easy = 'difficulty';
    let normal = 'difficulty';
    let hard = 'difficulty';
    
    if (this.state.difficulty === 10) {
      easy += ' diff-selected';
    } else if (this.state.difficulty === 12) {
      normal += ' diff-selected';
    } else if (this.state.difficulty === 14) {
      hard += ' diff-selected';
    }
    
    return (
      <div ref='inputDiv' tabIndex={0} className="splash-component" onKeyUp={this.handleKeyUp} >
        <img src={bg} />
        <p>Realign the faces to win!</p>
        <p>Game controls: use WASD or arrows, Up/Down changes the strip, Left/Right rotates the reel</p>
        <p>Press enter to start</p>
        <div>
          <span>Difficulty:</span>
          <ul>
            <li className={easy}>Easy</li>
            <li className={normal}>Normal</li>
            <li className={hard}>Hard</li>
          </ul>
        </div>
        <div className='title-mugshot-imploder' >
          <div className='title-mugshot-bouncer' >
            <img className='title-mugshot' src={mugshot} />
          </div>
        </div>
        <div className='title-mixup-imploder' >
          <div className='title-mixup-bouncer' >
            <img className='title-mixup' src={mixup} />
          </div>
        </div>
      </div>
    );
  }
}

SplashComponent.displayName = 'SplashComponent';

// Uncomment properties you need
SplashComponent.propTypes = {
  'starter': React.PropTypes.func
};
SplashComponent.defaultProps = {
  'starter': null
};

export default SplashComponent;
