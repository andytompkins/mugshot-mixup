
export default class Game {
  
  constructor(mugs, difficulty) {
    //console.log('game cons ' + difficulty);
    this.selected_reel = 1;
    this.difficulty = difficulty;
    this.slot = new Array(3);
    this.puzzle = this.create_puzzle(mugs, difficulty);
    //console.log("before spin");
    //this.debug();
    this.spin();
    //console.log("after spin");
    //this.debug();
    this.over = false;
    this.won = false;
  }
  
  reel_right() {
    if (!this.over) {
      this.rotate(this.selected_reel, 'right');
      this.detect_win();
    }
  }
  
  reel_left() {
    if (!this.over) {
      this.rotate(this.selected_reel, 'left');
      this.detect_win();
    }
  }
  
  reel_up() {
    if (!this.over) {
      if (this.selected_reel > 0) {
        this.selected_reel--;
      }
    }
  }
  
  reel_down() {
    if (!this.over) {
      if (this.selected_reel < 2) {
        this.selected_reel++;
      }
    }
  }
  
  detect_win() {
    //console.log('in detect winner');
    var slot0 = this.slot[0] + 1;
    var slot1 = this.slot[1] + 1;
    var slot2 = this.slot[2] + 1;
    if (this.puzzle[0][0].mug === slot0 && this.puzzle[1][0].mug === slot0 && this.puzzle[2][0].mug === slot0 &&
        this.puzzle[0][1].mug === slot1 && this.puzzle[1][1].mug === slot1 && this.puzzle[2][1].mug === slot1 &&
        this.puzzle[0][2].mug === slot2 && this.puzzle[1][2].mug === slot2 && this.puzzle[2][2].mug === slot2
    ) {
      //console.log('winner');
      this.game_over(true);
    } else {
      //console.log('lose');
    }
    
    
  }
  
  game_over(didWin) {
    this.over = true;
    this.won = didWin;
  }
  
  debug() {
    for (var i = 0; i < 3; i++) {
      var row = '|';
      for (var j = 0; j < this.puzzle[i].length; j++) {
        var x = this.puzzle[i][j].mug;
        if (x < 10) { x = '0' + x; }
        row += x + '|';
      }
      console.log(row);
    }
  }
  
  spin() {
    for (var i = 0; i < 3; i++) {
      var r = this.random(this.difficulty);
      for (var j = 0; j < r; j++) {
        this.rotate(i, 'right');
      }
    }
  }
  
  rotate(reel, direction) {
    if (direction === 'left') {
      this.puzzle[reel].push(this.puzzle[reel].shift());
    } else {
      this.puzzle[reel].unshift(this.puzzle[reel].pop());
    }
  }
  
  shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

      // Pick a remaining element...
      randomIndex = this.random(currentIndex);
      currentIndex -= 1;

      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  }
  
  random(exUpper) {
    return Math.floor(Math.random() * exUpper);
  }
  
  create_puzzle(mugs, difficulty) {
    //console.log('create puzzle called');
    //console.dir(mugs);
    var puz = new Array(3);
    for (var i = 0; i < 3; i++) {
      puz[i] = new Array(difficulty);
    }
  
 
    var mugnums = [];
    for (i = 0; i < mugs[0].length; i++) {
      mugnums.push(i);
    }
    //console.dir(mugnums);
    this.shuffle(mugnums);
    //console.dir(mugnums);
  
  
    this.slot[0] = mugnums.pop();
    this.slot[1] = mugnums.pop();
    this.slot[2] = mugnums.pop();
    
    //console.log('selected slots are ' + this.slot[0] + ', ' + this.slot[1] + ', ' + this.slot[2]);
  
    puz[0][0] = mugs[0][this.slot[0]];
    puz[1][0] = mugs[1][this.slot[0]];
    puz[2][0] = mugs[2][this.slot[0]];
  
    puz[0][1] = mugs[0][this.slot[1]];
    puz[1][1] = mugs[1][this.slot[1]];
    puz[2][1] = mugs[2][this.slot[1]];
  
    puz[0][2] = mugs[0][this.slot[2]];
    puz[1][2] = mugs[1][this.slot[2]];
    puz[2][2] = mugs[2][this.slot[2]];
  
    //console.dir(puz);
    
    var rest = new Array(3);
    for (i = 0; i < 3; i++) {
      rest[i] = new Array(mugnums.length);
      for (var j = 0; j < mugnums.length; j ++) {
        rest[i][j] = mugs[i][mugnums[j]];
      }
      this.shuffle(rest[i]);
    }
  
    for (i = 3; i < difficulty; i++) {
      puz[0][i] = rest[0][i - 3];
      puz[1][i] = rest[1][i - 3];
      puz[2][i] = rest[2][i - 3];
    }
  
    //console.dir(puz);
    return puz;
  }
  
}


